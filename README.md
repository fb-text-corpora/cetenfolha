# CETENFolha: Corpus de Texto

:warning: **NOTA**: o corpus retirado do antigo site do LaPS (2010)
encontra-se no arquivo [`res/ceten.xml.gz`](./res/), o qual contém 
aproximadamente 1 milhão e 543 mil sentenças após descompactado com `gzip`:

```bash
$ gzip -dk ceten.xml.gz
```

## Descrição
Descrição retirada de https://www.linguateca.pt/cetenfolha/index_info.html:

CETENFolha (**C**orpus de **E**xtractos de **T**extos **E**lectrónicos
**N**ILC/**Folha** de S. Paulo)
é um corpus de cerca de 24 milhões de palavras em português brasileiro, criado
pelo projecto Processamento computacional do português (projecto que deu origem
à Linguateca) com base nos textos do jornal Folha de S. Paulo que fazem parte do
corpus NILC/São Carlos, compilado pelo Núcleo Interinstitucional de Lingüística
Computacional (NILC).

- https://www.linguateca.pt/cetenfolha/index_info.html   
- https://www.linguateca.pt/cetenfolha/download/ 

## Downloading the source corpus file
Download the biggest file you se in the repository. Usually it'll appear with a
date that is close to the present day, e.g., in my case as of May 2020, the
filename was `CETENFolha-1.0_jan2014.cg.gz`. Then just extract using the command
below and _voilà_:

```bash
gzip -dk CETENFolha-1.0_jan2014.cg.gz
```

The file with `cg` extension is the one you'll be working on.

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/
