# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import re

class G1Pipeline:
    def process_item(self, item, spider):
        paragraphs = ''.join(item['text']) 
        text = re.sub(r'<.*?>', '', paragraphs)
        print(text)
