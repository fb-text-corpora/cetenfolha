# -*- coding: utf-8 -*-
import scrapy
from g1.items import G1Item

class G1politicaSpider(scrapy.Spider):
    name = 'g1Politica'
    page_number = 2
    allowed_domains = ['g1.globo.com']
    start_urls = ['http://g1.globo.com/politica']

    def parse(self, response):
        all_links = response.css("div.feed-media-wrapper a::attr(href)").extract() 
        for link in all_links:
            yield response.follow(link, self.parse_paragraph)
        next_page = response.css('div[class=\'load-more gui-color-primary-bg\'] a::attr(href)').extract_first()
       #Load more limit: https://www.youtube.com/watch?v=_5a_o3qcGOI
        if next_page is not None and G1politicaSpider.page_number <= 100000:
            G1politicaSpider.page_number += 1
            yield response.follow(next_page, self.parse)

    def parse_paragraph(self, response):
        if response.status == 404:
            pass
        article = response.css('article')
        paragraphs_with_tags = article.css('p').extract()
        paragraphs_with_tags = ''.join(paragraphs_with_tags)
        text = G1Item(text=paragraphs_with_tags)
        yield text
