#!/usr/bin/env python3
#
# script made by erick that catches some sentences from CETENFolha corpus that
# are not propely surrounded by sil opening-close pair of tags (<s>, </s>)
#
# author: may 2020
# erick campos

import sys
import re

def preprocessing(text):
    sentence = []
    for iten in text:
        phrase = []
        iten = iten.strip().replace('\n', '')
        iten = iten.strip().replace(':', '')
        iten = iten.strip().replace('=', ' ')
        iten = re.sub(r'<.*?>|\[.*?>\d|\$,.*?>\d|\d?\$[.]*|\(.*\)|#.*?>\d', '', iten)
        iten = re.sub(r' +', ' ', iten)
        iten = re.sub(r'\n+|\'|\"', '', iten)
        iten = ' '.join(iten.split())
        tokens = iten.lower().split()
        vowels = ('a','e','i','o','u')
        for word in tokens:
            if len(word) == 1 and not word.startswith(vowels):
                word = ''
            notNumber = re.search(r'^\d+[a-záàâãéèêíïóôõöúÁÀÂÃÉÈÍÏÓÔÕÖÚ]',word)
            if notNumber:
                word = re.sub(r'\d+', '', word)
            word = re.sub(r'^ *us *', '', word) #remove a string 'us'
            phrase.append(word)
        sentence.append(' '.join(phrase))
    sentence.pop(1)
    return sentence

if __name__=='__main__':
    if len(sys.argv) != 2:
        print('Usage: ')
        sys.exit(1)
        list = []
    with open(sys.argv[1], 'r', encoding='utf-8') as f:
        sTag = ''
        cont = 0
        for line in f:
            if line.startswith('</s>'):
                sTag += line
                list = sTag.split('</s>')
                sent = preprocessing(list)
                cont += 1
                print(sent)
                list.clear()
                sTag = ''
            else:
                sTag +=line
    print(cont)

