#!/usr/bin/env python3
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará
#
# author: feb 2019
# cassio batista - https://cassota.gitlab.io/
# last updated: may 2020

# NOTE: CB: last tested on May 2020 with file '1.2G CETENFolha-1.0_jan2014.cg'

import sys
import re
import locale
import datetime
try:
    from num2words.lang_PT_BR import Num2Word_PT_BR
except ImportError as e:
    print('%s: error: %s' % (sys.argv[0], e.msg))  # TODO CB: stack trace?
    print('please install "num2words" from', end=' ')
    print('https://github.com/savoirfairelinux/num2words')
    sys.exit(1)

import pattern
#import mapping

__author__ = 'Cassio Batista'
__email__ = 'cassio.batista.13@gmail.com'

class Number(Num2Word_PT_BR):
    """ Nice definition for class Number """

    # https://stackoverflow.com/questions/985505/locale-date-formatting-in-python
    locale.setlocale(locale.LC_ALL, ('pt_BR', 'UTF-8'))

    def __init__(self):
        super(Number, self).__init__()

    def parse_currency(self, val):
        """ TBD """

        pass


    def parse_time(self, val):
        """ Parse times """
        
        # https://stackoverflow.com/questions/2498935/how-to-extract-the-first-non-null-match-from-a-group-of-regexp-matches-in-python
        g = iter(val.groups())
        s = next(x for x in g if x is not None)
        h = next(x for x in g if x is not None)
        hour = self.to_cardinal(int(h))
        try:
            m = next(x for x in g if x is not None)
            if int(m) == 0:
                return '%s horas' % hour
            minutes = self.to_cardinal(int(m))
            return '%s horas e %s minutos' % (hour, minutes)
        except StopIteration:
            return '%s horas' % hour


    def parse_date(self, val):
        """ Parse dates """

        g = iter(val.groups())
        d = next(x for x in g if x is not None)
        m = next(x for x in g if x is not None)
        y = next(x for x in g if x is not None)
        day = self.to_cardinal(int(d))
        # https://stackoverflow.com/questions/6557553/get-month-name-from-number
        month = datetime.date(1900, int(m), 1).strftime('%B')
        if int(y) < 25:
            year = '20' + y
        year = self.to_cardinal(int(y))
        return '%s de %s de %s' % (day, month, year)


    def parse_normal(self, val):
        """ """
        # TODO CB: must detect large numbers separated by dots! (see patt_long)

        if ',' in val:
            number, decimals = val.split(',')
            number = self.to_cardinal(number)
            decimals = self.to_cardinal(decimals)
            return '%s vírgula %s' % (number, decimals)
        return self.to_cardinal(val)

    def parse_ordinal(self, val):
        number, char = val.groups()
        if number is None:
            print('what the hell')
            return val
        number = self.to_ordinal(int(number))
        if char == 'ª':
            female_number = []
            for word in number.split():
                female_number.append(re.sub(r'o$', 'a', word))
            return ' '.join(female_number)
        return number


    def process(self, pval, val, nval):
        """ receives val with a context of one """

        if pval == '#':
            pval = ''
        if nval == '#':
            nval = ''
        if val.isdigit():
            return pval, self.to_cardinal(int(val)), nval
        time = re.match(pattern.time, val)
        if time is not None:
            return pval, self.parse_time(time), nval
        date = re.match(pattern.date, val)
        if date is not None:
            return pval, self.parse_date(date), nval
        ordinal = re.match(pattern.ordinal, val)
        if ordinal is not None:
            return pval, self.parse_ordinal(ordinal), nval
        ulong = re.match(pattern.long_int, val)
        if ulong is not None:
            if any([g is None for g in ulong.groups()]):
                print('what the fuck is this', ulong)
                return pval, val, nval
            return pval, self.to_cardinal(int(val.replace('.', ''))), nval
        return pval, val, nval
        ## TODO CB: parse money not ready, so move this return to the
        ## R$ 11.500,12  -> onze mil e quinhentos reais e doze centavos
        ## US$ 11.500,12 -> onze mil e quinhentos dólares e doze centavos
        #if pval in map_currency.keys():
        #    return pval, val, nval
        #elif nval in map_units.keys():
        #    return pval, self.parse_normal(val), map_units[nval]
        #return pval, val, nval
