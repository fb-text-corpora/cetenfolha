#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará
#
# author: feb 2019
# cassio batista - https://cassota.gitlab.io/
# last updated: may 2020

# NOTE: CB: last tested on May 2020 with file '1.2G CETENFolha-1.0_jan2014.cg'

import sys
import os
import signal
import re
import time
from queue import Queue
from threading import Thread

# TODO CB: aspell python?
#      https://github.com/WojciechMula/aspell-python
#      https://pypi.org/project/aspell-python-py3/

import pattern
import mapping
from number import Number

START_TIME = time.time()

# https://stackoverflow.com/questions/3620943/measuring-elapsed-time-with-the-time-module
# https://stackoverflow.com/questions/1112343/how-do-i-capture-sigint-in-python
def trap_sigint(sig, frame):
    elapsed_time = time.time() - START_TIME
    print('\naborted. elapsed time:', elapsed_time)
    time.sleep(0.1)
    sys.exit(0)

signal.signal(signal.SIGINT, trap_sigint)

def postprocess_sent(sent):
    # treat preposition + [pronoun|article] e.g. de + [a] || de + [este]
    # NOTE: CB this will ignore concats that still are 'glued' by the
    #       '=' char, which will be split only after this mapping.
    for key in mapping.preposition:
        sent = re.sub(r'\b%s\b' % key, mapping.preposition[key], sent)
    fb_num = Number()
    sent = '# %s #' % sent.replace('- ','-').replace('=', ' ').strip()
    sent = sent.split()
    for i in range(1, len(sent) - 1):
        # https://stackoverflow.com/questions/19859282/check-if-a-string-contains-a-number
        if bool(re.search(r'\d', sent[i])):
            sent[i - 1], sent[i], sent[i + 1] = fb_num.process(sent[i - 1],
                                                               sent[i],
                                                               sent[i + 1])
    sent = ' '.join(sent)
    return sent.strip('#').strip()

def preprocess_sent(chunk_str):
    # https://stackoverflow.com/questions/640001/how-can-i-remove-text-within-parentheses-with-a-regex
    chunk_str = re.sub(r'\$\([^)]*\$\)', '', chunk_str) # parentheses
    chunk_str = re.sub(r'\$\[[^)]*\$\]', '', chunk_str) # brackets
    chunk = chunk_str.split('\n')
    if re.search(pattern.skip, chunk[0]) is not None:
        return None
    sentence = []
    while chunk:
        line = chunk.pop(0)
        # FIXME CB: cannot lower case here because if Aspell
        #       is gonna be implemented then proper nouns
        #       and other stuff needs to be capitalize
        #       for the spell checking to work.
        tokens = line.split('\t')
        if len(tokens) > 1:
            # FIXME CB: cannot break '=' here because sentences like
            #       e.g. 'a chance de a moeda desvalorizar' do not have
            #       prep + art 'de a' always joined to 'da'.
            word = tokens[0].split()
            # FIXME CB: cannot ignore all '$' here because sentences
            #       like 'US$ 2' or 'R$ 1,50' shall be translated
            #       into 'dollars' and 'reais' (currency) later.
            for w in word:
                sentence.append(w)
    if len(sentence) > 2:
        return ' '.join(sentence)
    else:
        return None

def divide(delim='-'):
    for i in range(150):
        print(delim, end='')
    print()

if __name__=='__main__':
    if len(sys.argv) != 2:
        print('Usage: %s <file.cg>' % sys.argv[0])
        sys.exit(1)

    sentences = []
    start_sil = False
    n = 35236585
    with open(sys.argv[1], 'r', encoding='utf-8') as f:
        for i, line in enumerate(f):
            if not start_sil and line.startswith('<s>'):
                chunk = ''
                start_sil = True
                continue
            if start_sil:
                if line.startswith('</s>'):
                    sent = preprocess_sent(chunk)
                    #print(sent, '#########################')
                    if sent is not None:
                        sent = postprocess_sent(sent)
                        #print(sent, '#########################')
                        if sent is not None:
                            sentences.append(sent)
                            print('%08d (%5.2f%%)' % (i, 100.0 * i / n), end=' ')
                            print(sent)
                            divide()
                    start_sil = False
                else:
                    chunk += line
    elapsed_time = time.time() - START_TIME
    print('elapsed time', elapsed_time)
