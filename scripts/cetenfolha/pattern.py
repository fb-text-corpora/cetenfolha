#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# a file to store regular expressions
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará
#
# author: may 2020
# cassio batista - https://cassota.gitlab.io/
# last updated: may 2020

import re

# https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s06.html
re_hour = '(2[0-3]|[01]?[0-9])' # 20-23 | 00-09, 10-19
re_min  = '([0-5]?[0-9])(h?)'
time = re.compile(r'^(%s:%s|%sh|%sh%s)$' % (re_hour, re_min,
                                            re_hour,
                                            re_hour, re_min))

# NOTE supports only dd/mm/[yy|yyyy]
re_day        = '(3[0-1]|[12][0-9]|0?[1-9])'
re_month      = '(1[0-2]|[0]?[1-9])'
re_year       = '(1[4-9]|2[0-1])([0-9][0-9])'
re_short_year = '([09][0-9])'
date = re.compile(r'^%s/%s/(%s|%s)$' % (re_day, re_month, re_year,
                                        re_short_year))

# https://stackoverflow.com/questions/34865975/regex-how-to-catch-a-number-with-commas-separating-the-thousands
# https://stackoverflow.com/questions/16148034/regex-for-number-with-decimals-and-thousand-separator
# 3.000.000
re_long_int =  r'^(\d{1,3})(\.\d{3})*$'
long_int = re.compile(re_long_int)

# 23.454.698,00
re_long_real = r'^(\d{1,3})(\.\d{3})*(,\d+)?$'
long_real = re.compile(re_long_real)

# 23,454,698.00
re_long_dollar = r'^(\d{1,3})(,\d{3})*(\.\d+)?$'
long_dollar = re.compile(re_long_dollar)

re_ordinal = r'(\d+)([ªº°])'
ordinal = re.compile(re_ordinal)

skip = r'^<a|^<li|^<s|^<t'
