
## G1
```
pip3 install scrapy --user
```
### Execução do script 
Vá até a página "spiders" localizado em um dos seus projetos. Por exemplo:

```
cd g1/g1/spiders
```
Um spider é o elemento que permite realizar o "crawler" no site específico,
realizando o fluxo necessário na navegação do site. Para executar um spider
digite o seguinte comando:

```
scrapy runspider nome_do_spider.py
```

[![FalaBrasil](../doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](../doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__     
__Universidade Federal do Pará__     
Erick Campos - ERICKCAMPOSWEIBSITE     
